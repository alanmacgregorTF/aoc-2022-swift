//
//  DayResultHelper.swift
//  AdventOfCode2021
//
//  Created by Alan MacGregor  on 06/11/2021.
//

import Foundation

protocol DayResult {
    static var day: Int { get }
    static func part1() -> String
    static func part2() -> String
}

extension DayResult {
    static func part1() -> String {
        "-"
    }
    
    static func part2() -> String {
        "-"
    }
    
    static func getFile() -> [String] {
        return DataHandler.getFileContents(forDay: day)
    }
}

struct DayResultHelper {
    static let maxResultLength = 25
    
    static func generateLine(for day: DayResult.Type, dayIndex index: Int, part: Int) -> String {
        let pre = Date()
        let result = part == 1 ? day.part1() : day.part2()
        if result.count > maxResultLength {
            fatalError("Result length needs extending to \(result.count)")
        }
        
        let time = Date().timeIntervalSince(pre)
        return ["| \(String(format: "%3i", index))",
                "Part\(part)",
                "\(result.padding(toLength: maxResultLength, withPad: " ", startingAt: 0))",
                "\(result == "-" ? "-      " : time.toTimeString()) |"]
            .joined(separator: " | ")
    }
    
    static func generateResults(for days: [DayResult.Type]) {
        print("+-----+-------+---------------------------+---------+")
        print("| Day | Part  | Result                    | Time    |")
        print("+-----+-------+---------------------------+---------+")
        let startTime = Date()
        for (index, day) in days.enumerated() {
            print(DayResultHelper.generateLine(for: day, dayIndex: index + 1, part: 1))
            print(DayResultHelper.generateLine(for: day, dayIndex: index + 1, part: 2))
        }
        
        let time = Date().timeIntervalSince(startTime)
        print("+-----+-------+---------------------------+---------+")
        print("                                          | \(time.toTimeString()) |")
        print("                                          +---------+")
    }
}

fileprivate extension TimeInterval {
    func toTimeString() -> String {
        return String(format: "%2f", self).padding(toLength: 7, withPad: "0", startingAt: 0)
    }
}
