//
//  DataHandler.swift
//  AdventOfCode2020
//
//  Created by Alan MacGregor  on 02/12/2020.
//

import Foundation

struct DataHandler {
    
    static func getFileContents(forDay day: Int) -> [String] {
        let currentDirectoryURL = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
        let bundleURL = URL(fileURLWithPath: "AOCBundle.bundle", relativeTo: currentDirectoryURL)
        let bundle = Bundle(url: bundleURL)
        
        let file = String(format: "Day%02i", day)
        let url = bundle!.url(forResource: file, withExtension: "txt")!
        let lines = try! String(contentsOf: url)
            .components(separatedBy: "\n")
            
        // Remove the last line if empty
        return lines.last!.isEmpty ? lines.dropLast() : lines
    }
}
