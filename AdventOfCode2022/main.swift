//
//  main.swift
//  AdventOfCode2020
//
//  Created by Alan MacGregor  on 01/12/2020.
//

import Foundation

let days: [DayResult.Type] = [
    Day01.self,
    Day02.self,
    Day03.self,
    Day04.self,
    Day05.self,
    Day06.self,
    Day07.self,
    Day08.self,
    Day09.self,
    Day10.self,
    Day11.self,
    Day12.self,
    Day13.self
]

DayResultHelper.generateResults(for: days)
