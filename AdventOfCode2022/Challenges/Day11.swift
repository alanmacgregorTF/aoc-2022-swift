//
//  Day11.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 11/12/2022.
//

import Foundation

struct Day11: DayResult {
    static let day = 11
    
    class Monkey {
        
        let number: Int
        var startingItems: [Int]
        let operation: ((Int) -> (Int))
        let monkeyToMoveTo: ((Int) -> (Int))
        var monkeyInspection = 0
        let divisibleNumber: Int
        
        init(from input: [String]) {
            number = input[0]
                .replacingOccurrences(of: ":", with: "")
                .replacingOccurrences(of: "Monkey ", with: "")
                .map({ Int(String($0))! })[0]
            startingItems = input[1]
                .replacingOccurrences(of: "  Starting items: ", with: "")
                .components(separatedBy: ", ")
                .map({ Int($0)! })
            let chars = input[2]
                .replacingOccurrences(of: "  Operation: new = old ", with: "")
                .components(separatedBy: " ")
            
            operation = { valInput in
                let rightSideValue = chars[1] == "old" ? valInput : Int(chars[1])!
                switch chars[0] {
                case "*": return valInput * rightSideValue
                case "+": return valInput + rightSideValue
                default: fatalError()
                }
            }
            
            let divisibleNumber = Float(input[3].replacingOccurrences(of: "  Test: divisible by ", with: ""))!
            let trueValidation = Int(input[4].replacingOccurrences(of: "    If true: throw to monkey ", with: ""))!
            let falseValidation = Int(input[5].replacingOccurrences(of: "    If false: throw to monkey ", with: ""))!
            self.divisibleNumber = Int(divisibleNumber)
            monkeyToMoveTo = { valInput in
                let test = Float(valInput).truncatingRemainder(dividingBy: divisibleNumber) == 0
                return test ? trueValidation : falseValidation
            }
        }
        
        func inspectItems(with otherMonkeys: [Monkey], divideBy3: Bool) {
            let overFill = otherMonkeys
                .map({ $0.divisibleNumber })
                .reduce(1, *)
            
            for item in startingItems {
                var item = item
                
                item = operation(item)
                // Bored
                if divideBy3 {
                    item = Int(floor(Double(exactly: item)! / 3))
                } else {
                    item = item % overFill
                }
                let monkeyToBeSentTo = monkeyToMoveTo(item)
                otherMonkeys[monkeyToBeSentTo].startingItems.append(item)
            }
            monkeyInspection += startingItems.count
            startingItems = []
        }
    }
    
    static func processMonkeys(from input: [String], divideBy3: Bool, rounds: Int) -> Int{
        let monkeys = Array(input.split(separator: ""))
            .map({ Monkey(from: Array($0)) })
        
        for _ in 0..<rounds {
            monkeys.forEach({ $0.inspectItems(with: monkeys, divideBy3: divideBy3) })
        }
        
        return monkeys
            .map { $0.monkeyInspection }
            .sorted(by: { $0 > $1 })[0..<2].reduce(1, *)
    }
    
    static func part1() -> String {
        processMonkeys(from: Self.getFile(), divideBy3: true, rounds: 20).description
    }
    
    static func part2() -> String {
        processMonkeys(from: Self.getFile(), divideBy3: false, rounds: 10000).description
    }
}
