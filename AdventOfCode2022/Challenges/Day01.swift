//
//  Day01.swift
//  AdventOfCode2020
//
//  Created by Alan MacGregor  on 01/12/2021.
//

import Foundation

struct Day01: DayResult {
    
    struct ElfItem {
        let index: Int
        let calories: Int
    }
    
    static var day = 1
    
    static func part1() -> String {
        getTopCaloriesElves(from: Self.getFile(), limit: 1).first!.calories.description
    }
    
    static func part2() -> String {
        Day01.getTopCaloriesElves(from: Self.getFile(), limit: 3).totalCalories.description
    }
    
    static func generateElves(from input: [String]) -> [ElfItem] {
        input.split(separator: "")
            .reduce([], { array, vals in
                let total = vals.map({ Int($0)! }).reduce(0, +)
                return array + [total]
            })
            .enumerated()
            .map({ ElfItem(index: $0.offset + 1, calories: $0.element) })
    }
    
    static func getTopCaloriesElves(from input: [String], limit: Int) -> [ElfItem] {
        let elves = generateElves(from: input)
            .sorted(by: { $0.calories > $1.calories })
            .prefix(limit)
        return Array(elves)
    }
}

extension Array where Element == Day01.ElfItem {
    var totalCalories: Int {
        self.map({ $0.calories }).reduce(0, +)
    }
}
