//
//  Day03.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 03/12/2022.
//

import Foundation

struct Day03: DayResult {
    static var day = 3
    static var alphabet = "abcdefghijklmnopqrstuvwxyz"
    static let fullAlphabet = Day03.alphabet + Day03.alphabet.uppercased()
    
    static func part1() -> String {
        Self.getFile()
            .sliceIntoTwoParts()
            .getTotal()
            .description
    }
    
    static func part2() -> String {
        Self.getFile()
            .groupIntoThrees()
            .getTotal()
            .description
    }
}

extension Array where Element == [String] {
    func getMatchingChars() -> [String.Element] {
        return self.map { item in
            Day03.fullAlphabet.first(where: { char in
                item.allSatisfy({ $0.contains(char) })
            })!
        }
    }
    
    func getTotal() -> Int {
        self
            .getMatchingChars()
            .getNumber()
            .reduce(0, +)
    }
}

extension Array where Element == String {
    func sliceIntoTwoParts() -> [[String]]  {
        self.map({ row in
            let length = Int(floor(Float(row.count) / 2))
            let midIndex = row.index(row.startIndex, offsetBy: length)
            let slice1 = row[row.startIndex..<midIndex].description
            let slice2 = row[midIndex..<row.endIndex].description
            return [slice1, slice2]
        })
    }
    
    func groupIntoThrees() -> [[String]] {
        return stride(from: 0, to: count, by: 3).map {
            Array(self[$0 ..< $0 + 3])
        }
    }
}

extension Array where Element == String.Element {
    func getNumber() -> [Int] {
        self.map { char in
            let index = Day03.fullAlphabet.firstIndex(of: char)!
            return Day03.fullAlphabet.distance(from: Day03.fullAlphabet.startIndex, to: index) + 1
        }
    }
}
