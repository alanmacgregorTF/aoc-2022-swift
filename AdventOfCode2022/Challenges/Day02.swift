//
//  Day02.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 02/12/2022.
//

import Foundation

struct Day02: DayResult {
    static var day = 2
    
    enum Result {
        case win, lose, draw
        
        init(string: String) {
            switch string {
            case "X":
                self = .lose
            case "Y":
                self = .draw
            case "Z":
                self = .win
            default:
                fatalError()
            }
        }

        func getChoice(from choice: Choice) -> Day02.Choice {
            switch self {
            case .draw:
                return choice
            case .lose:
                switch choice {
                case .rock: return .scissors
                case .paper: return .rock
                case .scissors: return .paper
                }
            case .win:
                switch choice {
                case .rock: return .paper
                case .paper: return .scissors
                case .scissors: return .rock
                }
            }
        }
    }
    
    enum Choice: Equatable {
        case rock, paper, scissors
        
        init(string: String) {
            switch string {
            case "A","X":
                self = .rock
            case "B","Y":
                self = .paper
            case "C","Z":
                self = .scissors
            default:
                fatalError()
            }
        }
        
        var value: Int {
            switch self {
            case .rock: return 1
            case .paper: return 2
            case .scissors: return 3
            }
        }
        
        func getResult(from choice: Choice) -> Day02.Result {
            switch self {
            case .rock:
                switch choice {
                case .rock: return .draw
                case .paper: return .lose
                case .scissors: return .win
                }
            case .paper:
                switch choice {
                case .rock: return .win
                case .paper: return .draw
                case .scissors: return .lose
                }
            case .scissors:
                switch choice {
                case .rock: return .lose
                case .paper: return .win
                case .scissors: return .draw
                }
            }
        }
    }
    
    static func generateChoices(input: [String]) -> [(Choice, Choice)] {
        input.compactMap({ row in
            let array = row.components(separatedBy: " ").map({ Choice(string: $0) })
            return (array[0], array[1])
        })
    }
    
    static func generateChoicesAndResult(input: [String]) -> [(Choice, Day02.Result)] {
        input.compactMap({ row in
            let items = row.components(separatedBy: " ")
            return (Choice(string: items[0]), Day02.Result(string: items[1]))
        })
    }
    
    static func calculateScore(from choices: (Choice, Choice)) -> Int {
        let (lhs, rhs) = choices
        switch rhs.getResult(from: lhs) {
        case .win: return rhs.value + 6
        case .lose: return rhs.value
        case .draw: return rhs.value + 3
        }
    }
    
    static func part1() -> String {
        Day02.generateChoices(input: Self.getFile())
            .map({ Day02.calculateScore(from: $0)})
            .reduce(0, +)
            .description
    }
    
    static func part2() -> String {
        Day02.generateChoicesAndResult(input: Self.getFile())
            .map({ ($0.0, $0.1.getChoice(from: $0.0)) })
            .map({ Day02.calculateScore(from: $0)})
            .reduce(0, +)
            .description
    }
}
