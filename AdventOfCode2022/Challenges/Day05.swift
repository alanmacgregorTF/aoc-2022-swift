//
//  Day05.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 05/12/2022.
//

import Foundation

struct Day05: DayResult {
    static let day = 5
    
    struct Instruction: Equatable {
        let move: Int
        let from: Int
        let to: Int
    }
    
    static func generateCrates(from input: [String]) -> [[String.Element]] {
        var array: [[String.Element]] = []
        for (index, row) in input.reversed().enumerated() {
            guard index != 0 else { continue }
            
            let pieces = (row.count / 4) + 1
            let chars = Array(row)
            
            for charIndex in 0..<pieces {
                let piece = chars[(charIndex * 4) + 1]
                guard piece != " " else { continue }
                if array.count > charIndex {
                    array[charIndex].append(piece)
                } else {
                    array.append([piece])
                }
            }
        }

        return array
    }
    
    static func generateInstructions(from input: [String]) -> [Instruction] {
        return input
            .map({ $0.components(separatedBy: " ") .compactMap({ Int($0) }) })
            .compactMap({ Instruction(move: $0[0], from: $0[1], to: $0[2]) })
    }
    
    static func getTopCrates(from input: [String], part1Configuration: Bool) -> String {
        var crates = generateCrates(from: Array(input.split(separator: "")[0]))
        let instructions = generateInstructions(from: Array(input.split(separator: "")[1]))
        
        for instruction in instructions {
            if part1Configuration {
                for _ in 0..<instruction.move {
                    crates[instruction.to - 1].append(crates[instruction.from - 1].removeLast())
                }
            } else {
                let cratesToAdd = crates[instruction.from - 1].suffix(instruction.move)
                crates[instruction.to - 1] += cratesToAdd
                crates[instruction.from - 1].removeLast(instruction.move)
            }
        }
        
        return String(crates.compactMap({ $0.last }))
    }
    
    static func part1() -> String {
        getTopCrates(from: Self.getFile(), part1Configuration: true)
    }
    
    static func part2() -> String {
        getTopCrates(from: Self.getFile(), part1Configuration: false)
    }
}
