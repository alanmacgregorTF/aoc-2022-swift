//
//  Day13.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 13/12/2022.
//

import Foundation

struct Day13: DayResult {
    static let day = 13
    
    enum Equality {
        case equal
        case greaterThan
        case lesserThan
    }
    
    indirect enum ArrayRepresentation: Equatable {
        case array([ArrayRepresentation])
        case number(Int)
        
        func equal(rhs: Self) -> Equality {
            let lhs = self
            
            switch (lhs, rhs) {
            case (let .number(lhsNumber), let .number(rhsNumber)):
                guard lhsNumber != rhsNumber else { return .equal }
                return lhsNumber < rhsNumber ? .lesserThan : .greaterThan
            
            case (let .array(lhsArray), let .array(rhsArray)):
                
                for index in 0..<lhsArray.count {
                    // Right side ran out of items, so inputs are not in the right order
                    guard index < rhsArray.count else {
                        return .greaterThan
                        
                    }
                    switch lhsArray[index].equal(rhs: rhsArray[index])  {
                    case .lesserThan: return .lesserThan
                    case .greaterThan: return .greaterThan
                    case .equal: break
                    }
                }
                
                if lhsArray.count == rhsArray.count {
                    return .equal
                }
                return .lesserThan
            case (.array, .number):
                return lhs.equal(rhs: .array([rhs]))
            case (.number, .array):
                return Self.array([lhs]).equal(rhs: rhs)
            }
        }
        
        init(from line: String) {
            let withoutBracket = String(line.dropFirst().dropLast())
            var brackets = 0
            var sections: [ArrayRepresentation] = []
            var createdString = ""
            var insideBracket = ""
            for char in withoutBracket {
                
                if char == "[" {
                    brackets += 1
                } else if char == "]" {
                    brackets -= 1
                    if brackets == 0 {
                        insideBracket += "]"
                        sections.append(.init(from: insideBracket))
                        insideBracket = ""
                    }
                } else if brackets == 0 {
                    if char == "," {
                        if let intVal = Int(createdString) {
                            sections.append(.number(intVal))
                        }
                        createdString = ""
                    } else {
                        createdString += String(char)
                    }
                }
                
                if brackets > 0 {
                    insideBracket += String(char)
                }
            }
            
            if let intValue = Int(createdString) {
                sections.append(.number(intValue))
            }
            
            self = .array(sections)
            
            if insideBracket != "" {
                fatalError()
            }
        }
    }
    
    static func totalValidGroups(input: [String]) -> Int {
        var total = 0
        
        for (index, item) in input.split(separator: "").enumerated() {
            let line1 = Array(item)[0]
            let line2 = Array(item)[1]
            
            let rep1 = ArrayRepresentation(from: line1)
            let rep2 = ArrayRepresentation(from: line2)
            switch rep1.equal(rhs: rep2) {
            case .equal, .greaterThan: break
            case .lesserThan:
                total += index + 1
            }
        }
    
        return total
    }
    
    static func dividerPacketLocation(input: [String]) -> Int {
        let newReps = ["[[2]]", "[[6]]"].map({ ArrayRepresentation(from: $0 )})
        
        let reps = input
            .filter({ $0 != "" })
            .map({ ArrayRepresentation(from: $0 )}) + newReps
        
        let sortedReps = reps
            .sorted(by: {
                switch $0.equal(rhs: $1) {
                case .greaterThan: return true
                case .lesserThan, .equal: return false
                }
            }).map{ $0 }
        
        let item1 = sortedReps.count - sortedReps.firstIndex(of: newReps[0])!
        let item2 = sortedReps.count - sortedReps.firstIndex(of: newReps[1])!
        
        return item1 * item2
    }
    
    static func part1() -> String {
        totalValidGroups(input: Self.getFile()).description
    }
    
    static func part2() -> String {
        dividerPacketLocation(input: Self.getFile()).description
    }
}
