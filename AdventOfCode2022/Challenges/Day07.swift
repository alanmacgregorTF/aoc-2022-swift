//
//  Day07.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 07/12/2022.
//

import Foundation

struct Day07: DayResult {
    static let day = 7
    
    class PathItem: Hashable {
        static func == (lhs: Day07.PathItem, rhs: Day07.PathItem) -> Bool {
            return lhs.name == rhs.name && lhs.previousDirectory == rhs.previousDirectory
        }
        
        let previousDirectory: PathItem?
        var directories: [PathItem] = []
        var fileSize: Int?
        let name: String
        
        init(name: String, previousDirectory: PathItem?) {
            self.name = name
            self.previousDirectory = previousDirectory
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(name)
            hasher.combine(previousDirectory)
        }
        
        var totalDirectorySize: Int {
            directories
                .map({ $0.totalDirectorySize })
                .reduce(0, +) + (fileSize ?? 0)
        }
    }
    
    static func generatePathItem(from input: [String]) -> PathItem {
        var splitRequests: [[String]] = []
        var currentRequests: [String] = []
        
        for line in input {
            if line.hasPrefix("$") {
                // New Request
                if !currentRequests.isEmpty {
                    splitRequests.append(currentRequests)
                    currentRequests = []
                }
            }
            currentRequests.append(line)
        }
        splitRequests.append(currentRequests)
        
        let mainPathItem = PathItem(name: "/", previousDirectory: nil)
        var currentPathItemLocation = mainPathItem
        
        for request in splitRequests {
            let task = request[0]
            if task.hasPrefix("$ cd") {
                switch task.components(separatedBy: " ").last {
                case "/":
                    while let directory = currentPathItemLocation.previousDirectory {
                        currentPathItemLocation = directory
                    }
                case "..":
                    currentPathItemLocation = currentPathItemLocation.previousDirectory!
                default:
                    let name = task.components(separatedBy: " ").last!
                    guard !currentPathItemLocation.directories.contains(where: { $0.name == name }) else {
                        fatalError()
                    }
                    let newDirectory = PathItem(name: name, previousDirectory: currentPathItemLocation)
                    currentPathItemLocation.directories.append(newDirectory)
                    currentPathItemLocation = newDirectory
                }
            } else if task.hasPrefix("$ ls") {
                for listItem in request[1..<request.count] {
                    if let fileSize = Int(listItem.components(separatedBy: " ").first!) {
                        let fileItem = PathItem(name: listItem.components(separatedBy: " ").last!, previousDirectory: currentPathItemLocation)
                        fileItem.fileSize = fileSize
                        currentPathItemLocation.directories.append(fileItem)
                    }
                }
            } else {
                fatalError()
            }
        }
        return mainPathItem
    }
    
    static func getDirectories(atMost: Bool, val: Int, from pathItem: PathItem) -> Set<PathItem> {
        var smallDirectories = Set<PathItem>()
        for directory in pathItem.directories {
            
            // Should be a directory and not a file
            guard directory.fileSize == nil else { continue }
            
            if atMost, directory.totalDirectorySize <= val {
                smallDirectories.insert(directory)
                
            } else if !atMost, directory.totalDirectorySize >= val {
                smallDirectories.insert(directory)
            }
                
            for newDirectory in getDirectories(atMost: atMost, val: val, from: directory) {
                smallDirectories.insert(newDirectory)
            }
        }
        return smallDirectories
    }
    
    static func part1() -> String {
        let pathItem = generatePathItem(from: Self.getFile())
        let smallDirectories = getDirectories(atMost: true, val: 100000, from: pathItem)
        return smallDirectories
            .map({ $0.totalDirectorySize })
            .reduce(0, +)
            .description
    }
    
    static func part2() -> String {
        let total = 70000000
        let update = 30000000
        let pathItem = generatePathItem(from: Self.getFile())
        let requiredSize = update - (total - pathItem.totalDirectorySize)
        
        let largeDirectories = getDirectories(atMost: false, val: requiredSize, from: pathItem)
        return largeDirectories
            .map { $0.totalDirectorySize }
            .sorted()
            .first!
            .description
    }
}
