//
//  Day09.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 09/12/2022.
//

import Foundation

struct Day09: DayResult {
    static let day = 9
    
    class Coord {
        var x: Int = 0
        var y: Int = 0
    }
    
    struct StructCoord: Hashable {
        let x: Int
        let y: Int
    }
    
    static func getMoves(from input: [String], knotCount: Int) -> Int {
        let routes = input.map { line in
            let item = line.components(separatedBy: " ")
            return (item[0], Int(String(item[1]))!)
        }
        
        var locations = Set<StructCoord>()
        
        var knots: [Coord] = []
        for _ in 0..<knotCount {
            knots.append(Coord())
        }

        for (direction, amount) in routes {
            for _ in 0..<amount {
                
                let topKnot = knots[0]
                
                switch direction {
                case "U": topKnot.y += 1
                case "R": topKnot.x += 1
                case "D": topKnot.y -= 1
                case "L": topKnot.x -= 1
                default: fatalError()
                }
                
                for knotIndex in 1..<knots.count {
                    let thisKnot = knots[knotIndex]
                    let lastKnot = knots[knotIndex - 1]
                    
                    if thisKnot.x + 2 == lastKnot.x && thisKnot.y + 2 == lastKnot.y {
                        thisKnot.x += 1
                        thisKnot.y += 1
                    } else if thisKnot.x + 2 == lastKnot.x && thisKnot.y - 2 == lastKnot.y {
                        thisKnot.x += 1
                        thisKnot.y -= 1
                    } else if thisKnot.x - 2 == lastKnot.x && thisKnot.y + 2 == lastKnot.y {
                        thisKnot.x -= 1
                        thisKnot.y += 1
                    } else if thisKnot.x - 2 == lastKnot.x && thisKnot.y - 2 == lastKnot.y {
                        thisKnot.x -= 1
                        thisKnot.y -= 1
                    } else if thisKnot.x + 2 == lastKnot.x {
                        // Right
                        thisKnot.x += 1
                        thisKnot.y = lastKnot.y
                    } else if thisKnot.x - 2 == lastKnot.x {
                        // Left
                        thisKnot.x -= 1
                        thisKnot.y = lastKnot.y
                    } else if thisKnot.y + 2 == lastKnot.y {
                        // Up
                        thisKnot.y += 1
                        thisKnot.x = lastKnot.x
                    } else if thisKnot.y - 2 == lastKnot.y {
                        // Down
                        thisKnot.y -= 1
                        thisKnot.x = lastKnot.x
                    }

                    if knotIndex == knots.count - 1 {
                        let newCoord = StructCoord(x: thisKnot.x, y: thisKnot.y)
                        locations.insert(newCoord)
                    }
                }
            }
        }

        return locations.count
    }
    
    static func part1() -> String {
        getMoves(from: Self.getFile(), knotCount: 2).description
    }
    
    static func part2() -> String {
        getMoves(from: Self.getFile(), knotCount: 10).description
    }
}
