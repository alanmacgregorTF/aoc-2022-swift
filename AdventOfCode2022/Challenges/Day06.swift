//
//  Day06.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 06/12/2022.
//

import Foundation

struct Day06: DayResult {
    static let day = 6
    
    static func getStartOfMarker(from input: [String], length: Int) -> Int {
        let input = input[0]
        
        var i = 0
        while true {
            let start = input.index(input.startIndex, offsetBy: i)
            let end = input.index(input.startIndex, offsetBy: i + (length - 1))
            let set = Set(input[start...end])
            guard set.count == length else { i += length - set.count; continue }
            return i + length
        }
        fatalError()
    }
    
    static func part1() -> String {
        getStartOfMarker(from: Self.getFile(), length: 4).description
    }
    
    static func part2() -> String {
        getStartOfMarker(from: Self.getFile(), length: 14).description
    }
}
