//
//  Day04.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 04/12/2022.
//

import Foundation

struct Day04: DayResult {
    static let day = 4
    
    static func generateRanges(from input: [String]) -> [[ClosedRange<Int>]] {
        input.map({
            $0
                .components(separatedBy: ",")
                .map({ $0.components(separatedBy: "-").map({ Int($0)! }) })
                .map({ $0[0]...$0[1] })
        })
    }
    
    static func part1() -> String {
        generateRanges(from: Self.getFile())
            .map({ $0.isOverlap(isFull: true) ? 1 : 0})
            .reduce(0, +)
            .description
    }
    
    static func part2() -> String {
        generateRanges(from: Self.getFile())
            .map({ $0.isOverlap(isFull: false) ? 1 : 0})
            .reduce(0, +)
            .description
    }
}

extension Array where Element == ClosedRange<Int> {
    func isOverlap(isFull: Bool) -> Bool {
        let range1 = self[0]
        let range2 = self[1]
        if isFull {
            return range1.isInside(of: range2) || range2.isInside(of: range1)
        } else {
            return range1.isPartialOverlap(of: range2) || range2.isPartialOverlap(of: range1)
        }
    }
}

extension ClosedRange where Bound == Int {
    func isInside(of range: Self) -> Bool {
        self.lowerBound >= range.lowerBound && self.upperBound <= range.upperBound
    }
    
    func isPartialOverlap(of range: Self) -> Bool {
        (self.lowerBound >= range.lowerBound && self.upperBound <= range.lowerBound) ||
        (self.lowerBound <= range.upperBound && self.upperBound >= range.upperBound)
    }
}
