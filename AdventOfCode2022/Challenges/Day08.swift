//
//  Day08.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 08/12/2022.
//

import Foundation

struct Day08: DayResult {
    static let day = 8
    
    struct Coords: Hashable {
        let x: Int
        let y: Int
        
        func coordGroups(length: Int) -> [[Coords]] {
            let upCoords = (0..<y).map({ Coords(x: x, y: $0) }).filter({ $0 != self }).reversed().map{ $0 }
            let downCoords = ((y + 1)..<length).map({ Coords(x: x, y: $0) }).filter({ $0 != self })
            let leftCoords = (0..<x).map({ Coords(x: $0, y: y) }).filter({ $0 != self }).reversed().map{ $0 }
            let rightCoords = ((x + 1)..<length).map({ Coords(x: $0, y: y) }).filter({ $0 != self })
            
            return [upCoords, downCoords, leftCoords, rightCoords]
        }
    }
    
    
    static func generateDict(from input: [String]) -> [Coords: Int] {
        input.enumerated().reduce(into: [:]) { (dict, enu) in
            let (yIndex, row) = enu
            for (xIndex, num) in Array(row).map({ Int(String($0))! }).enumerated() {
                dict[Coords(x: xIndex, y: yIndex)] = num
            }
        }
    }
    
    static func generateTopAreas(from input: [String]) -> Int {
        let dict = generateDict(from: input)
        var visibleTrees = 0
    
        for x in 0..<input.count {
            for y in 0..<input.count {
                let currentCoord = Coords(x: x, y: y)
                let currentVal = dict[currentCoord]!
                
                for coordArray in currentCoord.coordGroups(length: input.count) {
                    guard coordArray.contains(where: { dict[$0]! >= currentVal }) else { visibleTrees += 1; break }
                }
            }
        }
        
        return visibleTrees
    }
    
    static func generateBiggestScore(from input: [String]) -> Int {
        let dict = generateDict(from: input)
        var totalScores: [Int] = []
        for x in 0..<input.count{
            for y in 0..<input.count {
                let currentVal = dict[Coords(x: x, y: y)]!
                
                let currentCoord = Coords(x: x, y: y)
                var subTotal = 1
                for coordArray in currentCoord.coordGroups(length: input.count) {
                    var score = 0
                    for coord in coordArray {
                        score += 1
                        guard dict[coord]! < currentVal else { break }
                    }
                    subTotal *= score
                }
                totalScores.append(subTotal)
            }
        }
        return totalScores.max()!
    }
    
    
    static func part1() -> String {
        generateTopAreas(from: Self.getFile()).description
    }
    
    static func part2() -> String {
        generateBiggestScore(from: Self.getFile()).description
    }
}
