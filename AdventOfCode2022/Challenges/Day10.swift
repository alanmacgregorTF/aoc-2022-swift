//
//  Day10.swift
//  AdventOfCode2022
//
//  Created by Alan MacGregor on 11/12/2022.
//

import Foundation

struct Day10: DayResult {
    static let day = 10
    
    enum Tasks {
        case noop
        case addx(Int)
    }
    
    static func generateCalculatedValues(from input: [String]) -> [Int] {
        let tasks = input.map({ line in
            if line == "noop" {
                return Tasks.noop
            } else {
                return Tasks.addx(Int(line.components(separatedBy: " ")[1])!)
            }
        })

        var pendingAdds: [Int] = []
        for task in tasks {
            switch task {
            case .noop:
                pendingAdds.append(0)
            case .addx(let newAdd):
                pendingAdds.append(newAdd)
            }
        }
        
        var x = 1
        var calcedVals: [Int] = []
        var calcInProgress = true
        while !pendingAdds.isEmpty || calcInProgress {
            calcedVals.append(x)
            if calcInProgress {
                calcInProgress = false
                continue
            } else {
                let inputValue = pendingAdds.removeFirst()
                if inputValue != 0 {
                    calcInProgress = true
                    x += inputValue
                }
            }
        }
        
        return calcedVals
    }
    
    static func generateImage(from input: [String]) -> String {
        let calcedVals = generateCalculatedValues(from: input)
        var line = ""
        var index = 0
        
        for calcedVal in calcedVals {
            let ax = [calcedVal - 1, calcedVal, calcedVal + 1]
            line += ax.contains(index) ? "#" : " "
            index += 1
            if index == 40 {
                index = 0
            }
        }
 
        let locks = stride(from: 0, to: calcedVals.count - 1, by: 40).map {
            let startIndex = line.index(line.startIndex, offsetBy: $0)
            let endIndex = line.index(startIndex, offsetBy: 40)
            return line[startIndex..<endIndex]
        }
        
        return locks.joined(separator: "\n")
    }
    
    static func generateSignalStrengths(from input: [String]) -> Int {
        let calcedVals = generateCalculatedValues(from: input)

        let items =  [
            20 * calcedVals[20 - 1],
            60 * calcedVals[60 - 1],
            100 * calcedVals[100 - 1],
            140 * calcedVals[140 - 1],
            180 * calcedVals[180 - 1],
            220 * calcedVals[220 - 1]
        ]
        return items.reduce(0, +)
    }
    
    static func part1() -> String {
        Day10.generateSignalStrengths(from: Self.getFile()).description
    }
    
    static func part2() -> String {
        // Generates image rather than value
        return "EGJBGCFK (**MANUAL**)"
    }
}
