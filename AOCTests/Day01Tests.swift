//
//  Day01Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor  on 01/12/2021.
//

import XCTest

class Day01Tests: XCTestCase {
    let testInput = """
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
""".components(separatedBy: "\n")
    
    func testExample1() {
        XCTAssertEqual(Day01.getTopCaloriesElves(from: testInput, limit: 1)[0].index, 4)
    }
    
    func testExample2() {
        XCTAssertEqual(Day01.getTopCaloriesElves(from: testInput, limit: 3).totalCalories, 45000)
    }
    
    func testPart1() {
        XCTAssertEqual(Day01.part1(), "68775")
    }
    
    func testPart2() {
        XCTAssertEqual(Day01.part2(), "202585")
    }
}
