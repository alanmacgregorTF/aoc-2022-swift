//
//  Day06Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 06/12/2022.
//

import XCTest

final class Day06Tests: XCTestCase {
    let input = """
mjqjpqmgbljsphdztnvjfqwrcgsmlb
""".components(separatedBy: "\n")

    func testGetStartOfMarker() {
        XCTAssertEqual(Day06.getStartOfMarker(from: input, length: 4), 7)
        XCTAssertEqual(Day06.getStartOfMarker(from: input, length: 14), 19)
    }
    
    func testPart1() {
        XCTAssertEqual(Day06.part1(), "1929")
    }
    
    func testPart2() {
        XCTAssertEqual(Day06.part2(), "3298")
    }
}
