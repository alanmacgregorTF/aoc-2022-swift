//
//  Day08Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 08/12/2022.
//

import XCTest

final class Day08Tests: XCTestCase {


    let input = """
30373
25512
65332
33549
35390
""".components(separatedBy: "\n")
    
    func testGenerate() {
        XCTAssertEqual(Day08.generateTopAreas(from: input), 21)
    }
    
    func testPart1() {
        XCTAssertEqual(Day08.part1(), "1779")
    }
    
    func testPart2() {
        XCTAssertEqual(Day08.part2(), "172224")
    }
}
