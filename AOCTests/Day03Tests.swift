//
//  Day03Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 03/12/2022.
//

import XCTest

final class Day03Tests: XCTestCase {
    let input = """
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
""".components(separatedBy: "\n")


    func testGeneral() {
        XCTAssertEqual(Day03.alphabet.count, 26)
    }
    
    func testPart1() {
        XCTAssertEqual(Day03.part1(), "8153")
    }
    
    func testPart2() {
        XCTAssertEqual(Day03.part2(), "2342")
    }
}
