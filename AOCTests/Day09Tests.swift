//
//  Day09Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 09/12/2022.
//

import XCTest

final class Day09Tests: XCTestCase {

    let input1 = """
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
""".components(separatedBy: "\n")
    
    let input2 = """
R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
""".components(separatedBy: "\n")
    
    
    func testExample() {
        XCTAssertEqual(Day09.getMoves(from: input1, knotCount: 2), 13)
        XCTAssertEqual(Day09.getMoves(from: input1, knotCount: 10), 1)
        XCTAssertEqual(Day09.getMoves(from: input2, knotCount: 10), 36)
    }
    
    func testPart1() {
        XCTAssertEqual(Day09.part1(), "6026")
    }
    
    func testPart2() {
        XCTAssertEqual(Day09.part2(), "2273")
    }
}
