//
//  Day02Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 02/12/2022.
//

import XCTest

class Day02Tests: XCTestCase {
    
    let input = """
A Y
B X
C Z
""".components(separatedBy: "\n")

    func testInput() {
        let value = Day02.generateChoices(input: input)
            .map({ Day02.calculateScore(from: $0)})
            .reduce(0, +)
        XCTAssertEqual(value, 15)
    }
    
    func testInput1() {
        let value = Day02.generateChoicesAndResult(input: input)
            .map({ ($0.0, $0.1.getChoice(from: $0.0)) })
            .map({ Day02.calculateScore(from: $0)})
            .reduce(0, +)
        XCTAssertEqual(value, 12)
    }
    
    func testPart1() {
        XCTAssertEqual(Day02.part1(), "11873")
    }
    
    func testPart2() {
        XCTAssertEqual(Day02.part2(), "12014")
    }
}
