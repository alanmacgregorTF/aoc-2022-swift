//
//  Day13Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 13/12/2022.
//

import XCTest

final class Day13Tests: XCTestCase {

    let input = """
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]
""".components(separatedBy: "\n")
    
//    let input = """
//[1,[2,[3,[4,[5,6,7]]]],8,9]
//[1,[2,[3,[4,[5,6,0]]]],8,9]
//""".components(separatedBy: "\n")
    
    func testExample() {
        XCTAssertEqual(Day13.totalValidGroups(input: input), 13)
        XCTAssertEqual(Day13.dividerPacketLocation(input: input), 140)
    }

    func testPart1() {
        XCTAssertEqual(Day13.part1(), "5717")
    }
}
