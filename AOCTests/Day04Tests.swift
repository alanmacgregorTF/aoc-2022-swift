//
//  Day04Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 04/12/2022.
//

import XCTest

final class Day04Tests: XCTestCase {

    let input = """
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
""".components(separatedBy: "\n")
    
    func testPart1() {
        XCTAssertEqual(Day04.part1(), "503")
    }
    
    func testPart2() {
        XCTAssertEqual(Day04.part2(), "827")
    }
}
