//
//  Day07Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 07/12/2022.
//

import XCTest

final class Day07Tests: XCTestCase {

    let input = """
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
""".components(separatedBy: "\n")
    
    func testExample() {
        
        let pathItem = Day07.generatePathItem(from: input)
        let smallDirectories = Day07.getDirectories(atMost: true, val: 100000, from: pathItem)
        let val = smallDirectories
            .map({ $0.totalDirectorySize })
            .reduce(0, +)
        
        XCTAssertEqual(val, 95437)
        XCTAssertEqual(pathItem.totalDirectorySize, 48381165)
        XCTAssertEqual(70000000 - pathItem.totalDirectorySize, 21618835)
        XCTAssertEqual(30000000 - 21618835, 8381165)
    }
    
    func testPart1() {
        XCTAssertEqual(Day07.part1(), "1297683")
    }
    
    func testPart2() {
        XCTAssertEqual(Day07.part2(), "5756764")
    }
}
