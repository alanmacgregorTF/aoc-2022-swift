//
//  Day05Tests.swift
//  AOCTests
//
//  Created by Alan MacGregor on 05/12/2022.
//

import XCTest

final class Day05Tests: XCTestCase {

    static let input = """
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
""".components(separatedBy: "\n")
    
    func testGenerateCrates() {
        let newInput = Array(Self.input.split(separator: "")[0])
        XCTAssertEqual(Day05.generateCrates(from: newInput), [["Z","N"],["M","C","D"],["P"]])
    }
    
    func testGenerateInstructions() {
        let newInput = Array(Self.input.split(separator: "")[1])
        XCTAssertEqual(Day05.generateInstructions(from: newInput)[0], Day05.Instruction(move: 1, from: 2, to: 1))
    }
    
    func testGetTopCrates() {
        XCTAssertEqual(Day05.getTopCrates(from: Self.input, part1Configuration: true), "CMZ")
        XCTAssertEqual(Day05.getTopCrates(from: Self.input, part1Configuration: false), "MCD")
    }
    
    func testPart1() {
        XCTAssertEqual(Day05.part1(), "ZRLJGSCTR")
    }
    func testPart2() {
        XCTAssertEqual(Day05.part2(), "PRTTGRFPB")
    }
}
